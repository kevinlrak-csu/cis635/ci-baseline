#!/bin/bash

export src=${1:-`pwd`};
export build=${2:-${src}/build};

trim() { # from https://stackoverflow.com/a/3352015/4875896
    local var="$*"
    # remove leading whitespace characters
    var="${var#"${var%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   
    printf '%s' "$var"
}
export -f trim;

FindTestsFromExecutable() {
  local _test_executable="$1";
  local _test_case_list='[]';

  # Modeled after CMake's parsing of --gtest_list_tests, but omitting "pretty_suite" and "pretty_test"
  # see https://github.com/Kitware/CMake/blob/v3.19.1/Modules/GoogleTestAddTests.cmake#L69

  # CMake uses regular expressions to parse some of the output.
  # Bash can invoke utilities to process regular expressions, but that is slower than using bash's
  #   built-in extended glob expressions. We must first ensure those are enabled.
  #   See https://www.gnu.org/software/bash/manual/bash.html#Pattern-Matching
  shopt -s extglob; # Enable extended pattern matching operators.

  local _raw_test_list=();
  mapfile -t _raw_test_list < <(${_test_executable} --gtest_list_tests);
  local _suite=''; # CMake calls this `suite`, but googletest calls it `test case`
  for _line in "${_raw_test_list[@]}"; do
    if [ -z "${_line}" ]; then # Skip empty line
      continue;
    fi

    if [[ "${_line}" = *"gtest_main.cc" ]]; then # Skip header
      continue;
    fi

    # Do we have a module name or a test name?
    if [[ ! "${_line}" = " "* ]]; then
      # Module; remove trailing '.' and any comments to get just the name...
      _suite="${_line//.?(*( )#*)}" # Pattern should be equivalent to RegEx: "\.( *#.*)?"
    else
      # Test name; strip spaces and comments to get just the name...
      local _test="${_line//+( )}" # Pattern should be equivalent to RegEx: " +"
      _test="${_test//#*}" # Pattern should be equivalent to RegEx: "#.*"

      local _disabled=false;
      if [[ "${_suite}" = "DISABLED_"* ]] || [[ "${_test}" = "DISABLED_"* ]]; then
        _disabled=true;
      fi

      local _filter="${_suite}.${_test}";
      local _report_name="${src}/test_results/$(basename ${_test_executable} .test).${_filter////_}.gtest.xml";
      local _command="${_test_executable} --gtest_filter=\"${_filter}\" --gtest_output=\"xml:${_report_name}\"";

      _test_case_list=$(jq \
        --null-input \
        --arg _suite "${_suite}" \
        --arg _test "${_test}" \
        --arg _filter "${_filter}" \
        --arg _disabled "${_disabled}" \
        --arg _command "${_command}" \
        --argjson _test_case_list "${_test_case_list}" \
        '
          $_test_case_list | . |= (. + [
            {
              "suite": $_suite,
              "test": $_test,
              "filter": $_filter,
              "disabled": $_disabled,
              "command": $_command
            }
          ]) # add the current test to the object
        ');
    fi
  done

  echo "${_test_case_list}" | jq;
}
export -f FindTestsFromExecutable;

FindTests() {
  local _test_executables_obj='[]';

  # test executables are named ending in ".test" by make_target.cmake
  readarray -d '' _test_executables < <(find ${build}/bin -name '*.test' -print0);
  for _test_executable in ${_test_executables[@]}; do
    _test_executables_obj=$(jq \
      --null-input \
      --arg _test_executable "${_test_executable}" \
      --argjson _test_cases "$(FindTestsFromExecutable ${_test_executable})" \
      --argjson _test_executables_obj "${_test_executables_obj}" \
      '
        $_test_executables_obj | . |= (. + [
          {
            filename: $_test_executable,
            test_cases: $_test_cases
          }
        ]) # add the current executable to the object
      ');
  done

  echo "${_test_executables_obj}" | jq;
}
export -f FindTests;

ClearTestResults() {
  find ${src}/test_results \
    -name *.gtest.xml \
    -exec rm {} '+';
}
export -f ClearTestResults;
