#include "array"
#include "gtest/gtest.h"

// NOTE: Since our test unit contains an `int main` method, we can only test it by putting that into a namesapce.
// In order to do that, we have to include the source file directly, rather than any related header.
// This also ensures that the source is compiled with appropriate debugging symbols, optimizatopms, and gcov support.
namespace sample1 {
  #include "main.cpp"
}

namespace {

TEST(MainTest, MissingArg) {
  // Arrange
  std::array<const char*, 1> args = {
    "main"
  };

  // Act
  int result = sample1::main(args.size(), args.data());

  // Assert
  EXPECT_EQ(result, RETURN_FAILURE_MISSING_ARG);
}

TEST(MainTest, TooManyArgs) {
  // Arrange
  std::array<const char*, 3> args = {
    "main",
    "5",
    "something else"
  };

  // Act
  int result = sample1::main(args.size(), args.data());

  // Assert
  EXPECT_EQ(result, RETURN_FAILURE_ARG);
}

TEST(MainTest, HelpArg) {
  // Arrange
  std::array<const char*, 2> args = {
    "main",
    "--help"
  };

  // Act
  int result = sample1::main(args.size(), args.data());

  // Assert
  EXPECT_EQ(result, RETURN_SUCCESS);
}

TEST(MainTest, BadArg) {
  // Arrange
  std::array<const char*, 2> args = {
    "main",
    "unrecognized"
  };

  // Act
  int result = sample1::main(args.size(), args.data());

  // Assert
  EXPECT_EQ(result, RETURN_FAILURE_BAD_ARG);
}

TEST(MainTest, NumericArg) {
  // Arrange
  std::array<const char*, 2> args = {
    "main",
    "9"
  };

  // Act
  int result = sample1::main(args.size(), args.data());

  // Assert
  EXPECT_EQ(result, RETURN_SUCCESS);
}

}  // namespace
