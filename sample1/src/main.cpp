#include <iostream>
#include "return_codes.h"
#include "sample1.h"

void printHelp(const char* executable) {
  std::cout << "Usage: " << executable << " <number>" << std::endl;
}

int main(int argc, const char* argv[]) {
  const char* executable = argv[0];
  if (argc < 2) {
    std::cerr << "Missing <number> parameter." << std::endl;
    printHelp(executable);
    return RETURN_FAILURE_MISSING_ARG;
  }
  if (argc > 2) {
    std::cerr << "Too many parameters." << std::endl;
    printHelp(executable);
    return RETURN_FAILURE_ARG;
  }

  std::string param(argv[1]);
  if (param == "--help") {
    printHelp(executable);
    return RETURN_SUCCESS;
  }

  int paramInt = 0;
  try {
    paramInt = std::stoi(param);
  } catch (std::invalid_argument & e) {
    std::cerr << "Invalid parameter: " << param << std::endl;
    printHelp(executable);
    return RETURN_FAILURE_BAD_ARG;
  }

  std::cout << paramInt << "! is " << Factorial(paramInt) << std::endl;
  std::cout << paramInt << " is" << (IsPrime(paramInt) ? "" : " not") << " prime!" << std::endl;

  return RETURN_SUCCESS;
}
