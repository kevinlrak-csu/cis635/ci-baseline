#!/bin/bash

source findTests.sh
source coverage.sh

echo "pick -> pick_from_executable -> run_test"
echo "run_all"
echo "RunGcovr -> RunGcov -> RunGcovForObject"
echo "ClearCoverage"
echo "FindTests -> FindTestExecutables -> FindTestsFromExecutable"
echo "ClearTestResults"

declare -g tests_json;

run_all() {
  echo "Searching for test executables . . .";
  tests_json=$(FindTests);
  local _test_executables;
  mapfile -t _test_executables < <(echo "$tests_json" | jq -r '.[].filename');

  ClearTestResults;
  ClearCoverage;
  for _test_executable in ${_test_executables[@]}; do
    eval "${_test_executable}";
  done
  RunGcovr;
}

run_test() {
  local _test="${1}"
  local _command=$(echo "${_test}" | jq -r '.command');
  
  ClearTestResults;
  ClearCoverage;
  echo "Running: ${_command}";
  eval "${_command}";
  RunGcovr;
}

pick_from_executable() {
  local _executable=${1};
  echo "Searching for test cases in ${_executable} . . ."

  local _test_cases_json=$(echo "$tests_json" | jq -r \
    --arg _executable "${_executable}" \
    '.[] | select(.filename == $_executable) | .test_cases');

  local _test_filters;
  mapfile -t _test_filters < <(echo "${_test_cases_json}" | jq -r \
    '.[].filter');
  if [ ${#_test_filters[@]} -lt 1 ]; then
    echo "ERROR: Unable to locate any tests in ${_executable}.";
    return -1;
  fi

  declare -i _index;
  _index=0;
  for _test_filters in ${_test_filters[@]}; do
    _index=$(expr ${_index} + 1);
    printf "%3d)  %s\n" ${_index} ${_test_filters};
  done;
  printf "Select a number to execute that test: "
  read _index;
  if [ ${_index} -gt ${#_test_filters[@]} ]; then
    echo "ERROR: Invalid test selected.";
    return -1;
  fi

  local _test=$(echo "${_test_cases_json}" | jq -r \
    --arg _filter "${_test_filters[$(expr ${_index} - 1)]}" \
    '.[] | select(.filter == $_filter)');

  run_test "${_test}";
}

pick() {
  echo "Searching for test executables . . .";
  tests_json=$(FindTests);
  local _test_executables;
  mapfile -t _test_executables < <(echo "$tests_json" | jq -r '.[].filename');
  if [ ${#_test_executables[@]} -lt 1 ]; then
    echo "ERROR: Unable to locate any tests.";
    return -1;
  elif [ ${#_test_executables[@]} -eq 1 ]; then
    pick_from_executable "${_test_executables[0]}"
  else
    declare -i _index;
    _index=0;
    for _test_executable in ${_test_executables[@]}; do
      _index=$(expr ${_index} + 1);
      printf "%3d)  %s\n" ${_index} ${_test_executable};
    done;
    printf "Select a number to view tests in that executable: "
    read _index;
    if [ ${_index} -gt ${#_test_executables[@]} ]; then
      echo "ERROR: Invalid executable selected.";
      return -1;
    fi
    pick_from_executable "${_test_executables[$(expr ${_index} - 1)]}"
  fi
}
