#!/bin/bash

# Ensure container timezone matches local environment.
# The tzdata utility uses /etc/timezone and /etc/localtime to set the date.
cp `# .localTimeZone is created by devcontainer in initializeCommand` \
  `#SOURCE` .localTimeZone \
  `#DEST`   /etc/timezone;
rm -f .localTimeZone;

ln `# tzdata installs zoneinfo files for various timezones. We link to the appropriate one ` \
  --symbolic       `# make symbolic links instead of hard links` \
  --no-dereference `# treat LINK_NAME as a normal file if it is a symbolic link to a directory` \
  --force          `# remove existing destination files` \
  `#TARGET`    /usr/share/zoneinfo/$(cat /etc/timezone) \
  `#LINK_NAME` /etc/localtime;

apt-get  `# APT package handling utility` \
  update       `# resynchronize the package index files from their sources`;

apt-get  `# APT package handling utility` \
  --assume-yes `# automatic yes to all prompts` \
  dist-upgrade `# install the newest versions of all packages currently installed on the system` \
               `# also intelligently handles changing dependencies with new versions of packages`;

apt-get  `# APT package handling utility` \
  --assume-yes `# automatic yes to all prompts` \
  install      `# handle one or more packages desired for installation or upgrading` \
    openssh-client `# to use SSH with Git` \
    tzdata         `# to set timezone` \
    jq             `# to parse JSON output from gcov` \
    tzdata         `# to set timezone (automatically configured during installation)` \
    gdb            `# GNU debugger`;

python3 -m `# run Python library module as a script` \
  pip `# package-management system` \
    install    `# install packages` \
      --upgrade    `# upgrade all specified packages to the newest available version.` \
      pip          `# python package-management system` \
      gcovr        `# utility for generating summarized code coverage results from gcov`;
