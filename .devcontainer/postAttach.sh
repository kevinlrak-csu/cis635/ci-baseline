#!/bin/bash

# Linux containers with bind volumes from a Windows filesystem cannot always infer the correct ownership and mode for those files.

# This container should have two bind volumes:
`#1:` export SSH_DIR='/home/conan/.ssh';
    # defined in devcontainer.json to allow container processes to use host SSH configuration and keys
`#2:` export WORKSPACE_DIR=$(pwd);
    # defined by VSCode 'Remote - Containers' extension
    # called ${containerWorkspaceFolder} in devcontainer.json

# The Dockerfile ( https://github.com/conan-io/conan-docker-tools/blob/master/gcc_10/Dockerfile ) defines:
`# the user`  export DOCKER_USER=conan;
`# and group` export DOCKER_GROUP=1001;

# Some files in mounted volumes are incorrectly owned by root.
chown `# change file owner and group` \
  --recursive `# operate on files and directories recursively` \
  `#OWNER:GROUP` ${DOCKER_USER}:${DOCKER_GROUP} \
  `#FILES`       ${SSH_DIR} ${WORKSPACE_DIR};

# Typically directories can be read and searched (executed) by any user, but only written by their owner.
find ${WORKSPACE_DIR} `# search for files in a directory hierarchy` \
  -type d `# File is of type: directory` \
  -exec   `# Execute command` \
    chmod `# change file mode bits` \
      `#MODE`  u=rwx,g=rx,o=rx \
      `#FILES` '{}' \
    '+' `# '+' exec variant:` \
          `# each found filename is appended to a single parameter '{}'` \
          `# the command is executed once with the aggregated '{}' parameter`;

# Access to SSH_DIR should be restricted due to the sensitive nature of the private keys inside.
# The owner should be able to read/write/execute(search) all directories in SSH_DIR
find ${SSH_DIR} `# search for files in a directory hierarchy` \
  -type d `# File is of type: directory` \
  -exec   `# Execute command` \
    chmod `# change file mode bits` \
      `#MODE`   u=rwx,g-rwx,o-rwx `#700` \
      `#FILES` '{}' \
    '+' `# '+' exec variant:` \
          `# each found filename is appended to a single parameter '{}'` \
          `# the command is executed once with the aggregated '{}' parameter`;

# Initially treat all files in this SSH_DIR as if they are are confidential.
# Later we will increase visability to files that should be public.
find ${SSH_DIR} `# search for files in a directory hierarchy` \
  -type f `# File is of type: regular file` \
  -exec   `# Execute command` \
    chmod `# change file mode bits` \
      `#MODE`   u=rw,g-rwx,o-rwx `#600` \
      `#FILES` '{}' \
    '+' `# '+' exec variant:` \
          `# each found filename is appended to a single parameter '{}'` \
          `# the command is executed once with the aggregated '{}' parameter`;

# SSH_DIR contains SSH public keys and configuration files which should be readable by all users.
# (If file not found, ignore the error.)
chmod --quiet g+rw,o+r ${SSH_DIR}/config          || true;
chmod --quiet g+rw,o+r ${SSH_DIR}/authorized_keys || true;
chmod --quiet g+rw,o+r ${SSH_DIR}/known_hosts     || true;
chmod --quiet g+rw,o+r ${SSH_DIR}/*.pub           || true;
