#!/bin/bash

export src=${1:-`pwd`};
export build=${2:-${src}/build};

RunGcovForObject() {
  local object_file=${1};
  gcov                     \
    --demangled-names      \
    --relative-only        \
    --branch-counts        \
    ${object_file};
}
export -f RunGcovForObject;

RunGcov() {
  # There may be many test executables. Find and run each.
  find ${build}/CMakeFiles `# search for files in a directory hierarchy` \
    -path '*/test/*.cpp.o' `# File name matches shell pattern` \
    -exec   `# Execute command` \
      /bin/bash -c \
        `#command_string` 'cd ${build} && RunGcovForObject "$1"' \
        `#shell_name`     _  \
        `#parameters`     {} \
      ';' `# ';' exec variant:` \
            `# each found filename triggers the command to be executed` \
            `# the filename is provided to the command via the '{}' parameter`;
}
export -f RunGcov;

RunGcovr() {
  mkdir --parents ${src}/test_results;
  RunGcov;
  gcovr \
    --root          "${build}" `# The directory where gcc was invoked` \
    --filter        "${src}"       `# The directory which contains all source code` \
    --exclude       '.*/test/.*'               `# We don't really care about coverage of testing our test` \
    --use-gcov-files \
    --keep \
    --exclude-throw-branches \
    --xml           "${src}/test_results/gcovr.xml" \
    --html-details \
    --html-absolute-paths \
    --output        "${src}/test_results/gcovr.html";
  # VSCode Code Coverage Highlighter plugin has a problem displaying absolute paths in the XML...
  sed \
    --in-place \
    --expression='s/<source>\/workspaces\/ci-baseline\/build<\/source>/<source>\/workspaces\/ci-baseline<\/source>/g' \
    --expression='s/filename="\/workspaces\/ci-baseline\//filename="/g' \
    "${src}/test_results/gcovr.xml";
}
export -f RunGcovr;

ClearCoverage() {
  find ${build} \
    \( \
      -name *.gcov -o \
      -name *.gcda    \
    \) \
    -exec rm {} '+';
  find ${src}/test_results \
    \( \
      -name gcovr.xml -o \
      -name gcovr.*html  \
    \) \
    -exec rm {} '+';
}
export -f ClearCoverage;
