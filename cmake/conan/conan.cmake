
if(NOT COMMAND conan_cmake_run)
    if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
        message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
        file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.15/conan.cmake"
                        "${CMAKE_BINARY_DIR}/conan.cmake" 
                        TLS_VERIFY ON)
    endif()
    include(${CMAKE_BINARY_DIR}/conan.cmake)
endif()

conan_add_remote(
    NAME ci_baseline
    INDEX 1
    URL https://gitlab.com/api/v4/projects/21937343/packages/conan
    VERIFY_SSL True
)

file(RELATIVE_PATH CMAKE_CURRENT_LIST_DIR_RELATIVE ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_LIST_DIR})

conan_cmake_run(
    CONANFILE ${CMAKE_CURRENT_LIST_DIR_RELATIVE}/conanfile.txt
    BASIC_SETUP
    CMAKE_TARGETS
    BUILD missing
)
