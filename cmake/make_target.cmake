# Set RPath so executables can load libraries in the same directory even if it is moved.
set(CMAKE_BUILD_RPATH_USE_ORIGIN TRUE)
set(CMAKE_INSTALL_RPATH "$ORIGIN")
set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)

function(make_test TARGET_DIR)
    if(NOT TARGET tests)
        # Use Conan to download and include GoogleTest
        include(cmake/conan/conan.cmake)

        # Provide ability to use GoogleTest
        include(GoogleTest)

        # Custom target to trigger building all the test targets
        add_custom_target(tests)
    endif()

    add_target(EXECUTABLE ${TARGET_DIR}/test TEST_TARGET)
    add_dependencies(tests ${TEST_TARGET})

    set_target_properties(${TEST_TARGET} PROPERTIES
        EXCLUDE_FROM_ALL TRUE
        POSITION_INDEPENDENT_CODE TRUE
    )

    target_include_directories(${TEST_TARGET} PRIVATE
        ${TARGET_DIR}
        ${TARGET_DIR}/src
        ${TARGET_DIR}/include
    )

    target_link_libraries(${TEST_TARGET} PUBLIC CONAN_PKG::gtest)
    target_compile_options(${TEST_TARGET} PUBLIC
        -g           # Produce debugging information in the operating system's native format
        -O0          # Disable optimization which could change the flow of execution in the program
        --coverage   # compile code instrumented for coverage analysis. Synonym for -fprofile-arcs -ftest-coverage
    )
    target_link_options(${TEST_TARGET} PUBLIC
        --coverage   # link code instrumented for coverage analysis. Synonym for -lgcov
    )
    gtest_discover_tests(${TEST_TARGET} TEST_PREFIX "${TARGET_NAME}.")
endfunction()

function(build_target_namespace RESULT_VAR_NAME TARGET_PATH)
    set(${RESULT_VAR_NAME} "" PARENT_SCOPE)
    get_filename_component(TARGET_PATH_ABS ${TARGET_PATH} ABSOLUTE)
    get_filename_component(TARGET_PATH_DIRECTORY ${TARGET_PATH_ABS} DIRECTORY)
    get_filename_component(TARGET_PATH_NAME ${TARGET_PATH_ABS} NAME)

    if(${TARGET_PATH_ABS} STREQUAL ${CMAKE_SOURCE_DIR})
        get_filename_component(CMAKE_SOURCE_DIR_NAME ${CMAKE_SOURCE_DIR} NAME)
        set(${RESULT_VAR_NAME} "${CMAKE_SOURCE_DIR_NAME}" PARENT_SCOPE)
        return()
    endif()

    build_target_namespace(PARENT_DIR_NS ${TARGET_PATH_DIRECTORY})
    set(${RESULT_VAR_NAME} "${PARENT_DIR_NS}.${TARGET_PATH_NAME}" PARENT_SCOPE)
endfunction()

function(locate_target RESULT_VAR_NAME TARGET_PATH)
    set(${RESULT_VAR_NAME} "" PARENT_SCOPE)
    foreach(DIR in ${CMAKE_CURRENT_LIST_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_SOURCE_DIR})
        if(EXISTS ${DIR}/${TARGET_PATH})
            set(${RESULT_VAR_NAME} "${DIR}/${TARGET_PATH}" PARENT_SCOPE)
            return()
        endif()
    endforeach()
    set(${RESULT_VAR_NAME} "${TARGET_PATH}" PARENT_SCOPE)
endfunction()

function(add_target TARGET_TYPE TARGET_DIR)
    locate_target(TARGET_DIR ${TARGET_DIR})
    build_target_namespace(TARGET_NAME ${TARGET_DIR})
    message(STATUS "${TARGET_NAME} at ${TARGET_DIR}")

    # Optionally, return target name to caller
    if(${ARGC} GREATER 2)
        set(${ARGV2} "${TARGET_NAME}" PARENT_SCOPE)
    endif()

    if("${TARGET_TYPE}" STREQUAL "SHARED_LIBRARY")
        add_library(${TARGET_NAME} SHARED "")
    elseif("${TARGET_TYPE}" STREQUAL "STATIC_LIBRARY")
        add_library(${TARGET_NAME} STATIC "")
    elseif("${TARGET_TYPE}" STREQUAL "EXECUTABLE")
        add_executable(${TARGET_NAME} "")
    elseif("${TARGET_TYPE}" STREQUAL "INTERFACE")
        add_library(${TARGET_NAME} INTERFACE)
        target_include_directories(${TARGET_NAME} INTERFACE ${TARGET_DIR})
        file(GLOB_RECURSE TARGET_FILES ${TARGET_DIR}/*)
        target_sources(${TARGET_NAME} INTERFACE ${TARGET_FILES})
        return()
    else()
        message(FATAL_ERROR "This module requires TARGET_TYPE to be defined as either SHARED_LIBRARY, STATIC_LIBRARY, EXECUTABLE, or INTERFACE. [${TARGET_TYPE}]")
    endif()

    set_target_properties(${TARGET_NAME} PROPERTIES
        OUTPUT_NAME ${TARGET_NAME}
        ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
        LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
    )

    target_compile_options(${TARGET_NAME} PRIVATE
        -Wall            # enables all the warnings about constructions that some users consider questionable, and that are easy to avoid
        -Wextra          # enables some extra warning flags that are not enabled by -Wall
    )
    target_compile_features(${TARGET_NAME} PRIVATE
        cxx_std_17
        c_std_11
    )

    target_include_directories(${TARGET_NAME} PRIVATE ${TARGET_DIR})
    target_include_directories(${TARGET_NAME} PUBLIC  ${TARGET_DIR}/include)

    # Find source files
    file(GLOB         ${TARGET_NAME}_ROOT_SOURCES ${TARGET_DIR}/*.cpp    )
    file(GLOB_RECURSE ${TARGET_NAME}_SRC_SOURCES  ${TARGET_DIR}/src/*.cpp)
    target_sources(${TARGET_NAME} PRIVATE
        ${${TARGET_NAME}_ROOT_SOURCES}
        ${${TARGET_NAME}_SRC_SOURCES}
    )

    if(EXISTS ${TARGET_DIR}/test)
        make_test(${TARGET_DIR})
    endif()
endfunction()
